<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "Name : ". $sheep->name . "<br>";
echo "Legs : ". $sheep->legs . "<br>";
echo "Cold blooded : ". $sheep->cold_blooded . "<br><br>";


$buduk = new frog ("Buduk");
echo "Name : ". $buduk->name . "<br>";
echo "Legs : ". $buduk->legs . "<br>";
echo "Cold blooded : ". $buduk->cold_blooded . "<br>";
echo "Jump : ". $buduk -> jump(). "<br><br>";

$keraSakti = new ape ("Kera Sakti");
echo "Name : ". $keraSakti->name . "<br>";
echo "Legs : ". $keraSakti->legs . "<br>";
echo "Cold blooded : ". $keraSakti->cold_blooded . "<br>";
echo "Yell : ". $keraSakti -> yell();
?>