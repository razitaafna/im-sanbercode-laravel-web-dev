<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <form action="/signup" method="POST">
        @csrf
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <label>First Name</label><br><br>
    <input type="text" name="fname"><br><br>

    <label>Last Name</label><br><br>
    <input type="text" name="lname"><br><br>

    <label>Gender</label><br><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br><br>

    <label>Nationality</label><br><br>
    <select name="bahasa">
        <option value="">Indonesian</option>
        <option value="">India</option>
        <option value="">Malaysia</option>
    </select> <br><br>

    <label>Language Spoken</label><br><br>
    <input type="checkbox">Bahasa Indonesia<br>
    <input type="checkbox">English<br>
    <input type="checkbox">Other<br><br>

    <label>Bio</label><br><br>
    <textarea cols="10" rows="5"></textarea><br>
    
    <input type="submit" value="Sign Up">

</body>
</html>