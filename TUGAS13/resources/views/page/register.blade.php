@extends('layout.master')
@section('title')

Buat Account Baru!
@endsection

@section('content')
    <form action="/signup" method="POST">
        @csrf

    <h3>Sign Up Form</h3>

    <label>First Name</label><br><br>
    <input type="text" name="fname"><br><br>

    <label>Last Name</label><br><br>
    <input type="text" name="lname"><br><br>

    <label>Gender</label><br><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br><br>

    <label>Nationality</label><br><br>
    <select name="bahasa">
        <option value="">Indonesian</option>
        <option value="">India</option>
        <option value="">Malaysia</option>
    </select> <br><br>

    <label>Language Spoken</label><br><br>
    <input type="checkbox">Bahasa Indonesia<br>
    <input type="checkbox">English<br>
    <input type="checkbox">Other<br><br>

    <label>Bio</label><br><br>
    <textarea cols="10" rows="5"></textarea><br>
    
    <input type="submit" value="Sign Up">
@endsection